import sqlite3
from datetime import datetime

now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')


con = sqlite3.connect("scd_example.db")
cur = con.cursor()

cur.execute("""
    create table t1 (
      bk int
      , valid_from datetime default CURRENT_TIMESTAMP
      , valid_to datetime default '9999-12-31 23:59:59'
      , a1 varchar(20)
      , primary key (bk, valid_from)
    );
""")

cur.execute("""
    insert into t1 (bk, a1) values
        (1, 'A')
      , (2, 'B')
      , (3, 'C')
      , (4, 'D')
    ;
""")

res = cur.execute("select * from t1 where bk = 1;")
print("beforce scd: ", res.fetchall())

# On definie une nouvelle version de l'enregistrement (1, 'A')
bk = 1
a1 = 'Y'  # a1 a ete modifie

# On met a jour la date valid_to de l'enregistrement (1, 'A')
cur.execute('''
update t1
set valid_to = '{now}'
where bk in (
  select bk
  from t1
  where 1=1
    and valid_to = '9999-12-31 23:59:59' 
    and (
      -- "Outdated" records
      t1.bk = {bk}
      and t1.a1 <> '{a1}'
    )
)
;
'''.format(bk=bk, a1=a1, now=now))

# On insert la nouvelle version (1, 'Y')
cur.execute('''
insert into t1
 (
    bk
    , valid_from
    , valid_to
    , a1
  )
  select T.*
  from 
  (select {bk} bk, '{now}' valid_from, '9999-12-31 23:59:59' valid_to, '{a1}' a1) T
where not exists (
  select 1
  from t1
  where t1.bk = {bk}
  and t1.valid_to = '9999-12-31 23:59:59'
)
;
'''.format(bk=bk, a1=a1, now=now))

res = cur.execute("select * from t1 where bk = 1")
print("after scd: ", res.fetchall())
