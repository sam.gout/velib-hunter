# Velib' Hunter 🚲🔪

## Tuteur : Samuel GOUTIN

##  Objectif :
Ce projet vous propose d'explorer les données mises à disposition par OpenData Paris. En particulier, vous allez jouer avec les données présentant [la disponibilité des Velib' en temps réel](https://opendata.paris.fr/explore/dataset/velib-disponibilite-en-temps-reel).
La solution proposera des services pour permettre aux utilisateurs de trouver plus facilement un vélo, ou pour aider la ville de Paris à mieux gérer son parc de vélos.
Pour cela, vous aurez besoin de capturer et de stocker les données disponibles sur l'API d'OpenData Paris (et leur historique!) dans une base de données, puis de créer à votre tour une API pour y mettre à disposition vos services.

## Fonctionnalités obligatoires
- **F1** : obtenir le nom de la station la plus proche ayant au moins un vélo disponible à partir de coordonnées géographique. Vous pouvez utiliser l'[API d'Etalab](https://adresse.data.gouv.fr/api-doc/adresse) pour obtenir des coordonnées géographiques à partir de d'une adresse.
- **F2** : obtenir le nom de la station la moins fréquentée sur une période de temps données.
- **F3** : obtenir le numéro de l'arrondissement le plus fréquenté sur une période de temps données.

## Fonctionnalités optionnelles
- **F01** - *Recherche de vélo en temps réel* : Un utilisateur utilise la F1 pour trouver un vélo proche de lui. En arrivant sur place: mauvaise surprise, le vélo lui ai passé sous le nez. Pour éviter ce genre de désagréaments, le nom de la station la plus proche sera actualisé en temps réel. Pour cela, vous pouvez améliorer la F1 en proposant une connexion basé sur un WebSocket.
- **FO2** - *Déploiement* : Pour faciliter le déploiement de votre solution, conteneurisez-la en utilisant [Docker Compose](https://wiki-tech.io/Conteneurisation/Docker/Docker-Compose).
- **FO3** - *[CRUD](https://fr.wikipedia.org/wiki/CRUD)* : Permettez aux utilisateurs de manipuler votre base de données. Pour cela, complétez votre API en proposant des nouvelles routes permettant de :
    - récupérer toutes les informations d'une station
    - supprimer toutes les informations d'une station
    - mettre à jour le nom d'une station
    - ajouter une nouvelle station
    - récupérer la listre de toutes les stations
- **FO4** - *Historisation de la table **Station*** : Les caractéristique d'une Station peuvent être amenés à changer (comme son nom ou sa capacité). Utilisez une technique de modélisation adaptée pour historiser ces changements.

## Les outils
Pour répondre aux fonctionnalités demandés, vous aurez certainement besoin de vous armer de:
- une librairie pour construire des APIs (conseillé: FastAPI)
- une solution de base de données (conseillé: SQLite)
- un outil de versioning de code en équipe (Gitlab ou Github)
