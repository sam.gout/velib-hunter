from fastapi import FastAPI, WebSocket, Request
import asyncio
from fastapi.templating import Jinja2Templates
from datetime import datetime

app = FastAPI()

templates = Jinja2Templates(directory="scripts")

@app.get("/nearestbike/{adresse}")
async def get(request: Request, adresse: str):
    return templates.TemplateResponse("ws.html", {"request": request, "adresse": adresse})


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    data = await websocket.receive_text()
    while True:
        await websocket.send_text(f"Last position was: {data} ({datetime.now()})")
        await asyncio.sleep(2)
