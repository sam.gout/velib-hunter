from mock import patch
from fastapi import FastAPI
from fastapi.testclient import TestClient

app = FastAPI()
client = TestClient(app)


# Define an example method for each layer (dao, service and api).
# methods call each other (api_endpoint >call> service >call> dao)
def dao():
    return "dao function!"


def service():
    return "service function!" + dao()


@app.get("/endpoint")
async def api_endpoint():
    return {"msg": service()}


# Test each layer separately: api layer with mocked call to service,
# service layer with mocked call to dao layer and dao layer
@patch('tests.test_all.service')
def test_api_endpoint(mock_service):
    # prepare
    mock_service.return_value = "mocked service function!"
    expected = {"msg": "mocked service function!"}
    # act
    response = client.get("/endpoint")
    # assert
    assert response.status_code == 200
    assert response.json() == expected


@patch('tests.test_all.dao')
def test_service(mock_dao):
    # prepare
    mock_dao.return_value = " mocked dao function!"
    expected = "service function! mocked dao function!"
    # act
    result = service()
    # assert
    assert result == expected


def test_dao():
    # prepare
    expected = "dao function!"
    # act
    result = dao()
    # assert
    assert result == expected
